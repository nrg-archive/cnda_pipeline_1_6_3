#!/bin/bash

if test -z "$3"; then
  echo 1.0
  exit
fi

set +x

proj=$1
visit=$2
subjID=$3
subjLabel=$4
note=$5
user=$6
pass=$7
host=$8
zipfile=$9

shift 1
catalogPath=$9

resource_loc=${catalogPath}/ePrime/resources/

workdir=${proj}_${subjID}_${visit}_cbat

mkdir $workdir

mv $zipfile $workdir

cd $workdir

unzip -j -o `basename $zipfile`

for f in *.txt; do
  if echo "$f" | grep instructions; then
    continue;
  fi
  iconv -f UCS-2LE -t UTF-8 "$f" > iconvOut.txt
  echo .
  echo "$f"
  
  if echo "$f" | grep -q AssocRGN; then
	instructionFile=instructions-assoc.txt
  elif echo "$f" | grep -q CompSpan; then
	instructionFile=instructions-compspan.txt
  elif echo "$f" | grep -q CVandOE; then
	instructionFile=instructions-cvoepure.txt
  elif echo "$f" | grep -q CVOE; then
	instructionFile=instructions-cvoeswitch.txt
  elif echo "$f" | grep -q ReadSpan; then
	instructionFile=instructions-readspan.txt
  elif echo "$f" | grep -q semvef; then
	instructionFile=instructions-semvef.txt
  elif echo "$f" | grep -q simon; then
	instructionFile=instructions-simon.txt
  elif echo "$f" | grep -q Paper; then
	instructionFile=instructions-visspac.txt
  elif echo "$f" | grep -q Spatial; then
	instructionFile=instructions-visspac.txt
  fi
  
  echo $instructionFile
  
  perl ${resource_loc}eprime2xml ${resource_loc}${instructionFile} iconvOut.txt > dukeXML.xml
  
  xsltproc --stringparam project $proj --stringparam visit_id $visit --stringparam subject_ID $subjID --stringparam subject_Label $subjLabel --stringparam note "$note" ${resource_loc}cbat-format.xsl dukeXML.xml > xnatXML.xml
  
  label=`grep label xnatXML.xml | cut -d'"' -f8`
  echo $label
  
  restPath=${host}/REST/projects/${proj}/subjects/${subjID}/experiments/${label}
  
  # allow data deletion to prevent excess questions not getting overwritten for readspan/compspan
  #  since they have variable numbers of questions, while the others are fixed length-
  #  CVOE is stored in parts, so we want to limit that behavior to only readspan/compspan
  if [[ $instructionFile = instructions-readspan.txt || $instructionFile = instructions-compspan.txt ]]; then
	echo "Yes"
  	curl -u ${user}:${pass} -T xnatXML.xml "$restPath?allowDataDeletion=true&inbody=true"
  	echo "$restPath?allowDataDeletion=true&inbody=true"
  else
  	curl -u ${user}:${pass} -T xnatXML.xml "$restPath?inbody=true"
  	echo "$restPath?inbody=true"
  fi

  basefilename=`basename "$f" .txt`
  zipfilename=$basefilename
  zipfilename=`echo $zipfilename | tr " " "-"`
  zip "${zipfilename}.zip" "${basefilename}.txt" "${basefilename}.edat" "${basefilename}.edat2"
  
  restPath="${restPath}/resources/NEURO/files/${zipfilename}.zip?overwrite=true"
  curl -u ${user}:${pass} -F upload=@"${zipfilename}.zip" "$restPath"
  echo $restPath
done

rm *

cd -

# Commented out for now -- JG
# rmdir $workdir
