<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ipip="http://nrg.wustl.edu/ipip"
xmlns:xnat="http://nrg.wustl.edu/xnat">

<xsl:output indent="yes" />

<xsl:param name="project" />
<xsl:param name="visit_id" />
<xsl:param name="subject_ID" />
<xsl:param name="subject_Label" />
<xsl:param name="note" />

<!-- main template -->
<xsl:template match="/">
	<xsl:variable name="exp"><xsl:value-of select="events/params/value[@name='exp']" /></xsl:variable>
	<xsl:variable name="formType">
		<xsl:call-template name="selectFormType">
			<xsl:with-param name="expType" select="$exp" />
		</xsl:call-template>
	</xsl:variable>

	<xsl:element name="ipip:{$formType}" namespace="http://nrg.wustl.edu/ipip" xmlns:xnat="http://nrg.wustl.edu/xnat">
		<xsl:attribute name="project"><xsl:value-of select="$project" /></xsl:attribute>
		<xsl:attribute name="visit_id"><xsl:value-of select="$visit_id" /></xsl:attribute>
		<xsl:attribute name="label">
			<xsl:call-template name="generateLabel">
				<xsl:with-param name="subjLabel" select="$subject_Label" />
				<xsl:with-param name="visit" select="$visit_id" />
				<xsl:with-param name="formType" select="$formType" />
			</xsl:call-template>
		</xsl:attribute>
		
		<xsl:variable name="sessDate"><xsl:value-of select="events/params/value[@name='sessDate']" /></xsl:variable>
		<xsl:element name="xnat:date">
			<xsl:call-template name="parseEPrimeDate">
				<xsl:with-param name="dateString" select="$sessDate" />
			</xsl:call-template>
		</xsl:element>
		
		<xsl:variable name="sessTime"><xsl:value-of select="events/params/value[@name='sessTime']" /></xsl:variable>
		<xsl:element name="xnat:time">
			<xsl:call-template name="parseEPrimeTime">
				<xsl:with-param name="timeString" select="$sessTime" />
			</xsl:call-template>
		</xsl:element>
		
		<xsl:element name="xnat:note"><xsl:value-of select="$note" /></xsl:element>
		
		<xsl:element name="xnat:subject_ID"><xsl:value-of select="$subject_ID" /></xsl:element>
		
		<xsl:element name="ipip:source"><xsl:text>ePrime</xsl:text></xsl:element>
		
		<xsl:for-each select="events/event">
			<xsl:if test="value[@name='stimResp']">
				<xsl:variable name="pos" select="position()" />
				<xsl:element name="{concat('ipip:Q', $pos)}"><xsl:value-of select="value[@name='stimResp']" /></xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:element>
</xsl:template>

<!-- functions -->
<xsl:template name="selectFormType">
	<xsl:param name="expType" />
	
	<xsl:variable name="lcExpType" select="translate($expType, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefjhijklmnopqrstuvwxyz')" />
	
	<xsl:choose>
		<!-- Participant -->
		<xsl:when test="contains($lcExpType, 'participant')">
			<xsl:text>IPIPPT</xsl:text>
		</xsl:when>
		<!-- Collateral Source -->
		<xsl:when test="contains($lcExpType, 'collateral')">
			<xsl:text>IPIPCS</xsl:text>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="generateLabel">
	<xsl:param name="subjLabel" />
	<xsl:param name="visit" />
	<xsl:param name="formType" />
	
	<xsl:variable name="lcFormType" select="translate($formType, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefjhijklmnopqrstuvwxyz')" />
	
	<xsl:value-of select="concat($subjLabel, '_', $visit, '_', $lcFormType)" />
</xsl:template>

<xsl:template name="parseEPrimeDate">
	<xsl:param name="dateString" />
	
	<xsl:variable name="YYYY" select="substring($dateString, 7, 4)" />
	<xsl:variable name="MM" select="substring($dateString, 1, 2)" />
	<xsl:variable name="DD" select="substring($dateString, 4, 2)" />
	
	<xsl:value-of select="concat($YYYY, '-', $MM, '-', $DD)" />
</xsl:template>

<xsl:template name="parseEPrimeTime">
	<xsl:param name="timeString" />
	
	<xsl:choose>
		<!-- Time Format is H:MM:SS -->
		<xsl:when test="string-length($timeString) = 7">
			<xsl:variable name="hour" select="concat('0', substring-before($timeString, ':'))" />
			<xsl:value-of select="concat($hour, ':', substring-after($timeString, ':'))" />
		</xsl:when>
		<!-- Time Format is HH:MM:SS -->
		<xsl:when test="string-length($timeString) = 8">
			<xsl:choose>
				<!-- Midnight -->
				<xsl:when test="starts-with($timeString, 12)">
					<xsl:value-of select="concat('00:', substring-after($timeString, ':'))" />
				</xsl:when>				
				<!-- Not Midnight -->
				<xsl:otherwise>
					<xsl:value-of select="$timeString" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- Time Format is H:MM:SS PM or HH:MM:SS PM -->
		<xsl:otherwise>
			<xsl:choose>
				<!-- Noon -->
				<xsl:when test="starts-with($timeString, 12)">
					<xsl:value-of select="substring-before($timeString, ' PM')" />
				</xsl:when>
				<!-- Not Noon -->
				<xsl:otherwise>
					<xsl:variable name="adjustedHour" select="substring-before($timeString, ':') + 12" />
					<xsl:value-of select="concat($adjustedHour, substring($timeString, 3, 6))" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>