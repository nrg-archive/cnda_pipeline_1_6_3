import sys, os, re, argparse
from random import choice

versionNumber='1'
dateString='20140626_1308'
author='flavin'
progName=os.path.split(sys.argv[0])[-1]
idstring = '$Id: %s,v %s %s %s Exp $'%(progName,versionNumber,dateString,author)

PREFIXES = ['PREF','PREC','TEMP','GR','PAR','RAC','OCC','CAU','CER','BS']
COLORS = ['0:0:0','0:0:205','34:139:34','205:92:92','147:112:219','218:165:32','139:119:101','64:224:208','255:20:147','101:83:214']
REGIONS = ['prefrontal','precuneus','lattemp','gyrusrectus','parietal','rostral','occipital','caudate','cerebellum','brainstem']
REGION_RES = [re.compile(REGION) for REGION in REGIONS]

def main():
    #######################################################
    # PARSE INPUT ARGS
    parser = argparse.ArgumentParser(description='Generate plot config file from ROIs in manual ROI file')
    parser.add_argument('-v', '--version',
                        help='Print version number and exit',
                        action='version',
                        version=versionNumber)
    parser.add_argument('--idstring',
                        help='Print id string and exit',
                        action='version',
                        version=idstring)
    parser.add_argument('roiFilePath',
                        help='Path to text file defining ROIs')
    parser.add_argument('configPath',
                        help='Path to output plot config file.')
    args=parser.parse_args()

    configPath = args.configPath
    roiFilePath = args.roiFilePath
    #######################################################

    #######################################################
    # GET ROI NAMES FROM TEXT FILE
    # FIGURE OUT PREFIXES & COLORS, AND MATCH UP SUBREGIONS
    colorsWeHaveSeen = {}
    regionsWeHaveSeen = {}
    print 'Reading roi file {}.'.format(roiFilePath)
    with open(roiFilePath,'r') as f:
        for line in f:
            thisRegion = line.split()[0]
            print 'Attempting to match region {}.'.format(thisRegion)
            matchList = [REGION_RE.search(thisRegion) for REGION_RE in REGION_RES]
            if not any(matchList):
                # This region, listed in the text file, does not match a region in our pre-defined list
                # That is okay, we just will not have a nice abbreviation for it
                print 'Did not find a match. Adding to the list with prefix {}.'.format(thisRegion)
                regionsWeHaveSeen[thisRegion] = [thisRegion]
                colorsWeHaveSeen[thisRegion] = choice(COLORS)
            else:
                # This region is known to us.
                # Find which element it is in our region list
                for i,match in enumerate(matchList):
                    if match:
                        thisRegionID = i
                        break

                print 'Found a match with region {}. Adding to the list with prefix {}.'.format(REGIONS[thisRegionID],PREFIXES[thisRegionID])
                # Check if we have seen one of this region before (bc it may be a left- or right- subregion)
                if PREFIXES[thisRegionID] in regionsWeHaveSeen:
                    # Add this region to the list of subregions we have seen previously
                    # No need to add it to the colors list
                    regionsWeHaveSeen[PREFIXES[thisRegionID]].append(thisRegion)
                else:
                    # We have not seen this region before.
                    # Add a one-element list to the region dict, and add its color to the color dict
                    try:
                        colorsWeHaveSeen[PREFIXES[thisRegionID]] = COLORS[thisRegionID]
                    except IndexError:
                        colorsWeHaveSeen[PREFIXES[thisRegionID]] = choice(COLORS)
                    regionsWeHaveSeen[PREFIXES[thisRegionID]] = [thisRegion]

    #######################################################

    #######################################################
    # WRITE PLOT CONFIG FILE
    prefixList = regionsWeHaveSeen.keys()
    prefixLine = 'REGIONS=' + ','.join(prefixList)
    colorLineList = ['{}_COLOR={}'.format(prefix,colorsWeHaveSeen[prefix]) for prefix in prefixList]
    regionLineList = ['{}_FILES_SUFFIX={}'.format(prefix,','.join(regionsWeHaveSeen[prefix])) for prefix in prefixList]
    configText = prefixLine + '\n' + '\n'.join(colorLineList) + '\n' + '\n'.join(regionLineList)

    print 'Writing config file to {}.'.format(configPath)
    with open(configPath,'w') as f:
        f.write(configText)
    #######################################################


if __name__ == '__main__':
    print idstring
    main()

