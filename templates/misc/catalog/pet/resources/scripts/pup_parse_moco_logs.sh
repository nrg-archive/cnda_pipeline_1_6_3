#!/bin/bash -xe

VERSION=1.0
AUTHOR='Flavin'
DATESTRING='20140507'
if [ "$1" == "--version" ]; then
    echo $VERSION
    exit 0
fi

program=`basename $0`
idstr='$Id: '$program',v '$VERSION' '$DATESTRING' '$AUTHOR' $'
echo $idstr

JSON_FILE=$1
shift
MOCO_LOGS=( "$@" )

for log in ${MOCO_LOGS[*]}; do
    MOCO_ERROR=`grep 'ERROR =' $log | tr -s ' ' | cut -d ' ' -f 3`
    MOCO_ERROR_ARRAY=(${MOCO_ERROR_ARRAY[*]} $MOCO_ERROR)
done

MOCO_ERROR_CSV=`echo ${MOCO_ERROR_ARRAY[*]} | tr ' ' ','`
MOCO_ERROR_JSON='{"moco_error_list":['$MOCO_ERROR_CSV']}'

echo "Writing JSON $MOCO_ERROR_JSON to file $JSON_FILE"
echo $MOCO_ERROR_JSON > $JSON_FILE
