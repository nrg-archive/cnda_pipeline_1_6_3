#!/bin/bash -xe

VERSION=1.0
AUTHOR='Flavin'
DATESTRING='20140507'
if [ "$1" == "--version" ]; then
    echo $VERSION
    exit 0
fi

program=`basename $0`
idstr='$Id: '$program',v '$VERSION' '$DATESTRING' '$AUTHOR' $'
echo $idstr

JSON_FILE=$1
REG_LOG=$2

REG_ERROR=`grep 'ERROR =' $REG_LOG | tr -s ' ' | cut -d ' ' -f 3`

EXISTING_JSON=`sed -e 's/^{//' -e 's/}$//' $JSON_FILE`
NEW_JSON='"registration_error":'$REG_ERROR
JSON_STRING_TO_WRITE='{'$EXISTING_JSON','$NEW_JSON'}'

echo "Adding new JSON $NEW_JSON to existing JSON $EXISTING_JSON"
echo "Writing $JSON_STRING_TO_WRITE to file $JSON_FILE"
echo $JSON_STRING_TO_WRITE > $JSON_FILE
